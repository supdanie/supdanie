<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 9:38
 */

namespace Model;

use Model\Entities;

class StaticDatabase
{
    protected $_accounts = array();
    protected $_employees = array();
    protected $_functions = array();
    private static $instance = null;
    private function __construct () {
        $this->_employees = Entities\Employee::walk();
        $this->_accounts = Entities\Account::walk();
        $this->_functions = Entities\Role::walk();
   }
    public function getAccount($id) {
        if(!isset($this->_accounts[$id])){
            return null;
        }
        return $this->_accounts[$id];
    }
    public function getEmployee($id){
        if(!isset($this->_employees[$id])){
            return null;
        }
        return $this->_employees[$id];
    }
    public function getFunction($id){
        if(!isset($this->_functions[$id])){
            return null;
        }
        return $this->_functions[$id];
    }

    public function existsRole(Entities\Employee $employee, $role){
        $found = false;
        foreach ($employee->getRoles() as $roleObject){
            if(strcmp($roleObject->getTitle(), $role) == 0){
                $found = true;
                break;
            }
        }
        return $found;
    }
    public function getEmployeesByParameters($name, $surname, $role, $email, $phone, $web){
        $employees = array();
        foreach ($this->_employees as $employee){
            $ok = true;
            if(!empty($name)){
                $ok = strcmp($employee->getName(), $name) == 0;
            }
            if($ok && !empty($surname)){
                $ok = strcmp($employee->getSurname(), $surname) == 0;
            }
            if($ok == true && !empty($email)){
                $ok = strcmp($employee->getEmail(), $email) == 0;
            }
            if($ok == true && !empty($role)){
                $ok = $this->existsRole($employee, $role);
            }
            if($ok == true && !empty($phone)){
                $ok = strcmp($employee->getPhone(), $phone) == 0;
            }
            if($ok == true && !empty($web)){
                $ok = strcmp($employee->getWeb(), $web) == 0;
            }
            if($ok == true){
                $employees[$employee->getId()] = $employee;
            }
        }
        return $employees;
    }
    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new StaticDatabase();
        }
        return self::$instance;
    }
    public function addEmployee(Entities\Employee $employee){
        $this->_employees[$employee->getId()] = $employee;
    }
    public function addFunction(Entities\Role $role){
        $this->_functions[$role->getId()] = $role;
    }
    public function addAccount(Entities\Account $account){
        $this->_accounts[$account->getId()] = $account;
    }
    /**
     * @return array
     */
    public function getAccounts()
    {
        return $this->_accounts;
    }

    /**
     * @return array
     */
    public function getEmployees()
    {
        return $this->_employees;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return $this->_functions;
    }
}