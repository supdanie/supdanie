<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.17
 * Time: 8:12
 */

namespace Model\Entities;


trait SerializableTrait{
    public function serialize(){
        $properties = get_object_vars($this);
        $this->preSerialize($properties);
        $properties["_parent"] = parent::serialize();
        return serialize($properties);
    }
    public function unserialize($serialized){
        $properties = unserialize($serialized);
        parent::unserialize($properties["_parent"]);
        foreach ($properties as $property => $value){
            $this->$property = $value;
        }
    }
}