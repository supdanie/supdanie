<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 9:40
 */

namespace Model\Entities;


class Role extends Entity
{
    use SerializableTrait;
    private $title;
    private $description;
    private $isVisible;
    private $employees;
    public function __construct($title, $description, $isVisible)
    {
        parent::__construct();
        $this->title = $title;
        $this->description = $description;
        $this->isVisible = $isVisible;
        $this->employees = array();
    }

    /**
     * @return array
     */
    public function getEmployees()
    {
        return $this->employees;
    }
    public function addEmployee(Employee $employee){
        $this->employees[$employee->getId()] = $employee;
    }
    public function removeEmployee(Employee $employee){
        unset($this->employees[$employee->getId()]);
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param mixed $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    public function preSerialize($properties)
    {
        if(!empty($properties["employees"])){
            $properties["employees"] = array_map(function($employee){ return $employee->getId();}, $properties["employees"]);
        }
        return $properties;
    }
    public function postUnserialize()
    {
        if(!empty($this->employees)){
            $this->employees = array_map(function($employee){ return $employee instanceof Employee ? $employee : Entity::find($employee);}, $this->employees);
        }
    }
}