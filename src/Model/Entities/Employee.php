<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 9:39
 */

namespace Model\Entities;


class Employee extends Entity
{
    use SerializableTrait;
    private $name;
    private $surname;
    private $email;
    private $photo;
    private $phone;
    private $web;
    private $room;
    private $roles;
    private $accounts;
    private $description;
    public function __construct($name, $surname, $email, $photo, $phone = null, $web = null, $description = null, $room = null)
    {
        parent::__construct();
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->photo = $photo;
        $this->phone = $phone;
        $this->web = $web;
        $this->room = $room;
        $this->roles = array();
        $this->accounts = array();
        $this->description = $description;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $desciption
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }


    /**
     * @param null $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @param null $web
     */
    public function setWeb($web)
    {
        $this->web = $web;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param null $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @return array
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @return null
     */
    public function getWeb()
    {
        return $this->web;
    }
    public function addAccount(Account $account){
        array_push($this->accounts, $account);
    }
    public function addRole(Role $role){
        array_push($this->roles, $role);
    }

    public function removeAccount(Account $account){
        $key = array_search($account, $this->accounts);
        if($key !== false){
            unset($this->accounts[$key]);
        }
    }
    public function removeRole(Role $role){
        $key = array_search($role, $this->roles);
        if($key !== false){
            unset($this->roles[$key]);
        }
    }
    public function preSerialize($properties)
    {
        if(!empty($properties["roles"])){
            $properties["roles"] = array_map(function($role){ return $role->getId();}, $properties["roles"]);
        }
        if(!empty($properties["accounts"])){
            $properties["accounts"] = array_map(function($account){ return $account->getId();}, $properties["accounts"]);
        }
        return $properties;
    }
    public function postUnserialize()
    {
        if(!empty($this->roles)){
            $this->roles = array_map(function($role){ return $role instanceof Role ? $role : Entity::find($role);}, $this->roles);
        }
        if(!empty($this->accounts)){
            $this->accounts = array_map(function($account){ return $account instanceof Account ? $account : Entity::find($account);}, $this->accounts);
        }
    }
}