<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 9:39
 */

namespace Model\Entities;



class Account extends Entity
{
    use SerializableTrait;
    private $employee;
    private $username;
    private $password;
    private $validTo;

    public function __construct(Employee $employee, $username, $password, \DateTime $validTo = null)
    {
        parent::__construct();
        $this->employee = $employee;
        $this->username = $username;
        $this->password = $password;
        $this->validTo = $validTo;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }
    public function preSerialize($properties)
    {
        if(!empty($properties["employee"])){
            $properties["employee"] = $properties["employee"]->getId();
        }
        return $properties;
    }
    public function postUnserialize()
    {
        parent::postUnserialize();
        if(!empty($this->employee) && !($this->employee instanceof Employee)){
            $this->employee = Entity::find($this->employee);
        }
    }
}