<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.17
 * Time: 8:11
 */

namespace Model\Entities;

use League\Flysystem\Exception;
use Service\StorageService;

abstract class Entity implements \Serializable
{
    static $lastID=0;
    static $models = array();
    protected $id;
    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }
    public function __construct()
    {
        $this->id = $this->nextId();
        static::$lastID++;
    }
    public function save(){
        if(empty($this->id)){
            $this->id = $this->nextId();
        }
        $filesystem = StorageService::getInstance()->getFilesystem();
        $filesystem->put(static::getShortName() . "/" . $this->id . ".ser", serialize($this));
        static::$models[static::getShortName() . $this->id] = $this;
    }
    public function delete(){
        $filesystem = StorageService::getInstance()->getFilesystem();
        $filesystem->delete(static::getShortName() . "/" . $this->id . ".ser");
        unset(static::$models[static::getShortName() . $this->id]);
    }
    public static function find($id){
        if(isset($models[static::getShortName().$id])){
            return $models[static::getShortName().$id];
        }
        $filesystem = StorageService::getInstance()->getFilesystem();
        $object = unserialize($filesystem->read(static::getShortName()."/".$id.".ser"));
        static::$models[static::getShortName().$object->getId()] = $object;
        $object->postUnserialize();
        return $object;
    }
    public static function getShortName(){
        return (new \ReflectionClass(static::class))
            ->getShortName();
    }
    protected function nextId(){
        $filesystem = StorageService::getInstance()->getFilesystem();
        $id = static::$lastID;
        foreach ($filesystem->listContents(static::getShortName()) as $file){
            if($id < $file['filename']){
                $id = $file['filename'];
            }
        }
        return $id+1;
    }
    public static function walk(){
        $array = array();
        $filesystem = StorageService::getInstance()->getFilesystem();
        foreach ($filesystem->listContents(static::getShortName()) as $file) {
            if ($file["type"] == "file") {
                $object = static::find($file["filename"]);
                if ($object instanceof static) {
                    $array[$object->getId()] = $object;
                }
            }
        }
        return $array;
    }
    protected function preSerialize($properties){
        return $properties;
    }
    protected function postUnserialize(){
    }
    public function serialize()
    {
        return;
    }
    public function unserialize($serialized)
    {
    }
}