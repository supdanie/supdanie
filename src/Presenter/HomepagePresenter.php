<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 10:05
 */

namespace Presenter;


class HomepagePresenter extends BasePresenter
{
    public function getName()
    {
        return "index.php";
    }
    public function getTemplateName()
    {
        return "index.html";
    }

    public function getVariables()
    {
        return array('baseurl' => $this->getBaseURL());
    }
}