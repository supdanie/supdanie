<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.10.17
 * Time: 9:04
 */

namespace Presenter;


class NoEmployeeWithIDPresenter extends BasePresenter
{

    public function getName()
    {
        return "zadnyZamestnanecSID";
    }

    public function getTemplateName()
    {
        return "zadnyZamestnanecSID.html";
    }

    public function getVariables()
    {
        return array('baseurl' => $this->getBaseURL());
    }
}