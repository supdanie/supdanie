<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 10:08
 */

namespace Presenter;


abstract class BasePresenter
{
    public abstract function getName();
    public abstract function getTemplateName();
    public abstract function getVariables();
    public function getBaseURL(){
        $base = str_replace("index.php", "", "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
        if(isset($_SERVER['REMOTE_HOST'])){
            $base = str_replace("index.php", "", $_SERVER['REMOTE_HOST'].$_SERVER['PHP_SELF']);
        }
        if(substr($base, strlen($base)-3) == "//") {
            $base = substr($base, 0, strlen($base) - 1);
        }
        if(substr($base, strlen($base)-1) != "/") {
            $base = $base."/";
        }
        return $base;
    }
}