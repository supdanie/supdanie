<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 10:05
 */

namespace Presenter;


use Model\StaticDatabase;

class DetailPresenter extends BasePresenter
{
    public function getName()
    {
        return "detail";
    }
    public function getTemplateName()
    {
        return "detail.html";
    }

    public function getVariables()
    {
        $id = $_GET["id"];
        $employee = StaticDatabase::getInstance()->getEmployee(intval($_GET["id"]));
        if(empty($employee)){
            header("Location: index.php?page=zadnyZamestnanecSID&id=".$_GET["id"]);
        }
        return array('id' => $id, 'employee' => $employee, 'baseurl' => $this->getBaseURL());
    }
}