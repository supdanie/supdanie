<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 13:57
 */

namespace Presenter;


use Model\StaticDatabase;

class ListPresenter extends BasePresenter
{
    public function getName()
    {
        return "prehled";
    }
    public function getTemplateName()
    {
        return "prehled.html";
    }
    private static function name($a, $b){
        if(strcmp($a->getName(),$b->getName()) == 0){
            return 0;
        }
        return strcmp($a->getName(),$b->getName()) < 0 ? -1 : 1;
    }
    private static function surname($a, $b){
        if(strcmp($a->getSurname(),$b->getSurname()) == 0){
            return 0;
        }
        return strcmp($a->getSurname(),$b->getSurname()) < 0 ? -1 : 1;
    }
    private static function email($a, $b){
        if(strcmp($a->getEmail(),$b->getEmail()) == 0){
            return 0;
        }
        return strcmp($a->getEmail(),$b->getEmail()) < 0 ? -1 : 1;
    }
    private static function phone($a, $b){
        if(strcmp($a->getPhone(),$b->getPhone()) == 0){
            return 0;
        }
        return strcmp($a->getPhone(),$b->getPhone()) < 0 ? -1 : 1;
    }
    private static function web($a, $b){
        if(strcmp($a->getWeb(),$b->getWeb()) == 0){
            return 0;
        }
        return strcmp($a->getWeb(),$b->getWeb()) < 0 ? -1 : 1;
    }
    public function getVariables()
    {
        $database = StaticDatabase::getInstance();
        $name = isset($_GET["name"]) ? $_GET["name"] : null;
        $surname = isset($_GET["surname"]) ? $_GET["surname"] : null;
        $role = isset($_GET["role"]) ? $_GET["role"] : null;
        $email = isset($_GET["email"]) ? $_GET["email"] : null;
        $phone = isset($_GET["phone"]) ? $_GET["phone"] : null;
        $website = isset($_GET["website"]) ? $_GET["website"] : null;
        $employees = $database->getEmployeesByParameters($name, $surname, $role, $email, $phone, $website);
        if(isset($_GET["razeni"])){
            usort($employees, array("Presenter\\ListPresenter", $_GET["razeni"]));
        }
        return array('employees' => $employees, 'baseurl' => $this->getBaseURL());
    }
}