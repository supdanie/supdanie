<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.10.17
 * Time: 9:03
 */

namespace Presenter;


class NoPageFoundErrorPresenter extends BasePresenter

{

    public function getName()
    {
        return "strankaNeexistuje";
    }

    public function getTemplateName()
    {
        return "strankaNeexistuje.html";
    }

    public function getVariables()
    {
        return array('baseurl' => $this->getBaseURL());
    }
}