<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 14:03
 */

namespace Presenter;


use Model\Entities\Employee;
use Model\StaticDatabase;

class AddNewEmployee extends BasePresenter
{
    public function getName()
    {
        return "vlozeni";
    }

    public function getTemplateName()
    {
        return "vlozeni.html";
    }

    public function sendForm(){
        if(isset($_POST["add"])) {
            $name = $_POST["name"];
            $surname = $_POST["surname"];
            $email = $_POST["email"];
            $image = $_FILES["photo"]["name"];
            while(file_exists(__DIR__."/../../photo/".$image)){
                $image = "0".$image;
            }
            copy($_FILES["photo"]["name"], __DIR__."/../../photo/".$image);
            $phone = isset($_POST["phone"]) ? $_POST["phone"] : null;
            $web = isset($_POST["website"]) ? $_POST["website"] : null;
            $description = $_POST["description"];
            $employee = new Employee($name, $surname, $email, $image, $phone, $web, $description);
            $employee->save();
            StaticDatabase::getInstance()->addEmployee($employee);
        }
    }
    public function getVariables()
    {
        $error = "";
        if(isset($_POST["name"]) && empty($_POST["name"])){
            $error = $error."Je nutné vyplnit jméno\n";
        }
        if(isset($_POST["surname"]) && empty($_POST["surname"])){
            $error = $error."Je nutné vyplnit příjmení\n";
        }
        if(isset($_POST["email"]) && empty($_POST["email"])){
            $error = $error."Je nutné vyplnit e-mailovou adresu\n";
        }
        if(isset($_FILES["photo"]["name"]) && empty($_FILES["photo"]["name"])){
            $error = $error."Je nutné vybrat fotografii\n";
        }
        if($error == "") {
            if(isset($_POST["add"])) {
                 header("Location: index.php?page=prehled");
            }
            $this->sendForm();
        }
        return array('baseurl' => $this->getBaseURL(), 'error' => $error);
    }
}