<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 10:05
 */

namespace Presenter;


use Model\Entities\Role;
use Model\StaticDatabase;

class EditEmployeePresenter extends BasePresenter
{
    public function getName()
    {
        return "editace";
    }
    public function getTemplateName()
    {
        return "editace.html";
    }

    public function sendForm(){
        $isset = isset($_POST["edit"]) ? true : false;
        $removedRole = null;
        $employee = StaticDatabase::getInstance()->getEmployee(intval($_GET["id"]));
        foreach($employee->getRoles() as $role){
            if($isset != true && isset($_POST["removeRole".$role->getId()])){
                $isset = true;
                $removedRole = $role;
                break;
            }
        }
        if($isset == false){
            $isset = isset($_POST["addRole"]) ? true : false;
        }
        if($isset == true){
            $employee->setName($_POST["name"]);
            $employee->setSurname($_POST["surname"]);
            $employee->setEmail($_POST["email"]);
            $employee->setPhone($_POST["phone"]);
            $employee->setWeb($_POST["website"]);
            $employee->setDescription($_POST["description"]);
            if(!empty($_FILES["photo"]["name"])) {

                $image = $_FILES["photo"]["name"];
                while(file_exists(__DIR__."/../../photo/".$image)){
                    $image = "0".$image;
                }
                copy($_FILES["photo"]["name"], __DIR__."/../../photo/".$image);
                $employee->setPhoto($image);
            }
            foreach ($employee->getRoles() as $role){
                $role->setTitle($_POST["role".$role->getId()."Title"]);
                $role->setDescription($_POST["role".$role->getId()."Description"]);
                $role->setIsVisible(isset($_POST["role".$role->getId()."Visible"]));
                $role->save();
            }
            if(!empty($removedRole)){
                $employee->removeRole($role);
            }
            if(isset($_POST["addRole"])){
                $title = $_POST["roleNewTitle"];
                $description = $_POST["roleNewDescription"];
                $visible = isset($_POST["roleNewVisible"]);
                $role = new Role($title, $description, $visible);
                $role->save();
                $employee->addRole($role);
            }
            $employee->save();
        }
    }

    public function getVariables()
    {
        if(isset($_POST["edit"])){
            header("Location: index.php?page=detail&id=".$_GET["id"]);
        }
        $employee = StaticDatabase::getInstance()->getEmployee(intval($_GET["id"]));
        if(empty($employee)){
            header("Location: index.php?page=zadnyZamestnanecSID&id=".$_GET["id"]);
        }
        $this->sendForm();
        $employee = StaticDatabase::getInstance()->getEmployee(intval($_GET["id"]));
        return array('baseurl' => $this->getBaseURL(), 'employee' => $employee);
    }
}