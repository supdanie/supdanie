<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9.10.17
 * Time: 9:29
 */

namespace Router;

use Presenter\HomepagePresenter;

class Router
{
    static $DIR = __DIR__."/../Presenter/template";
    static $PRESENTERS = __DIR__."/../Presenter/";
    static $CACHE = __DIR__."/../";
    public $twig;
    private $presenterArray = array();
    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(self::$DIR);
        $this->twig = new \Twig_Environment($loader, array('debug' => true));
        $presenters = scandir(self::$PRESENTERS);
        foreach ($presenters as $presenter){
            if(!is_dir($presenter)) {
                $nameArray = explode(".", $presenter);
                $name = $nameArray[0];
                if($name != "BasePresenter" && $name != "template"){
                    $className = "Presenter\\" . $name;
                    $presenterObject = new $className();
                    $this->presenterArray[$presenterObject->getName()] = $name;
                }
            }
        }
        $this->presenterArray[""] = "HomepagePresenter";
    }
    public function route(){
        $presenter = "HomepagePresenter";
        if(isset($_GET["page"]) && isset($this->presenterArray[$_GET["page"]])){
            $presenter = $this->presenterArray[$_GET["page"]];
        }
        if(isset($_GET["page"]) && !isset($this->presenterArray[$_GET["page"]])){
            $presenter = "NoPageFoundErrorPresenter";
        }
        $class = "Presenter\\".$presenter;
        $object = new $class();
        echo $this->twig->render($object->getTemplateName(), $object->getVariables());
    }
}